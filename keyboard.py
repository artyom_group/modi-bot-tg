from aiogram.types import ReplyKeyboardMarkup, KeyboardButton


class Keyboard:
    def __init__(self):
        pass

    def __call__(self):
        kbrd = ReplyKeyboardMarkup(
            keyboard=[
                [
                    KeyboardButton(text="Перейти на сайт"),
                    KeyboardButton(text="Посмотреть каталог в телегараме")
                ]
            ]
        )
        return kbrd