from keyboard import Keyboard
import os

from aiogram import Bot, Dispatcher, executor, types

API_TOKEN = os.environ["TOKEN"]

bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot)


@dp.message_handler(commands=['start'])
async def send_welcome(message: types.Message):
    keyboard = Keyboard()
    await message.reply("Добро пожаловать в бот интернет-магазина MOFI", reply_markup=keyboard())


@dp.message_handler()
async def send_main_msg(message: types.Message):
    keyboard = Keyboard()
    if message.text == "Перейти на сайт":
        await message.answer("Наш сайт https://mofi.ru/", reply_markup=keyboard())
    else:
        await message.answer("Новинки в телеграме появляются раньше чем на сайте. Ссылка на канал https://t.me/onlinemofi", reply_markup=keyboard())


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)